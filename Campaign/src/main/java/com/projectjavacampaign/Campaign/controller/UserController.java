package com.projectjavacampaign.Campaign.controller;

import com.projectjavacampaign.Campaign.dto.response.UploadResponse;
import com.projectjavacampaign.Campaign.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Tag(
        name = "Upload Avatar User REST API for Users Resource",
        description = "Upload Avatar User Rest APIs - Upload Avatar User"
)
@RestController
@AllArgsConstructor
@RequestMapping("api/v1/users")
@PreAuthorize("hasRole('USER')")
public class UserController {

    UserService userService;

    @Operation(
            description = "Get endpoint for Upload User",
            summary = "This is a summary for User get endpoint",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Unauthorized / Invalid Token",
                            responseCode = "403"
                    )
            }

    )

    @PatchMapping(value = "/{userId}/upload-avatar", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @PreAuthorize("hasAuthority('USER_ROLE')")
    public UploadResponse uploadAvatar(
            @PathVariable Long userId,
            @RequestPart("avatarFileName") MultipartFile avatarFileName) throws IOException {

        return ResponseEntity.ok(userService.uploadAvatar(avatarFileName, userId)).getBody();
    }
}

