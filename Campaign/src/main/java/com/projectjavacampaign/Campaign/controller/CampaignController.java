package com.projectjavacampaign.Campaign.controller;

import com.projectjavacampaign.Campaign.dto.CampaignDTO;
import com.projectjavacampaign.Campaign.dto.CampaignImageDTO;
import com.projectjavacampaign.Campaign.dto.response.CampaignImageResponse;
import com.projectjavacampaign.Campaign.dto.response.CampaignResponseCreate;
import com.projectjavacampaign.Campaign.dto.response.ListCampaignResponse;
import com.projectjavacampaign.Campaign.dto.response.ResponseCampaign;
import com.projectjavacampaign.Campaign.service.CampaignService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Tag(
        name = "Create, Get All, Get By Id, Update, Upload Image Campaign REST API for Campaigns Resource"
)
@RestController
@AllArgsConstructor
@RequestMapping("api/v1/campaigns")
public class CampaignController {

    private CampaignService service;

    @Operation(
            summary = "Create or Register Campaign API",
            description = "Create Campaign REST API is used save campaign in a database"
    )
    @ApiResponse(
            responseCode = "201",
            description = "HTTP Status 201 CREATE"
    )
    @PostMapping("/")
    @PreAuthorize("hasAuthority('USER_ROLE')")
    public ResponseEntity<CampaignResponseCreate> createCampaign(@Valid @RequestBody CampaignDTO campaignDTO){
        CampaignResponseCreate response = service.createCampaign(campaignDTO);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Operation(
            summary = "Get All Campaign API",
            description = "Get All Campaign REST API is used egt campaign in a database"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP Status 200 OK"
    )
    @GetMapping("/getCampaign")
    public ResponseEntity<ListCampaignResponse> getCampaign(){
        ListCampaignResponse campaignAll = service.getCampaignAll();
        return new ResponseEntity<>(campaignAll, HttpStatus.OK);
    }

    @Operation(
            summary = "Get By Id Campaign API",
            description = "Get By Id Campaign REST API is used egt campaign in a database"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP Status 200 OK"
    )
    @GetMapping("{id}")
    public ResponseEntity<ResponseCampaign> getByIdCampaign(@PathVariable("id")
                                                            Long id){
        ResponseCampaign responseCampaign = service.getByID(id);
        return new ResponseEntity<>(responseCampaign, HttpStatus.OK);
    }

    @Operation(
            summary = "Update Campaign API",
            description = "Update Campaign REST API is used update campaign in a database"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP Status 200 OK"
    )
    @PutMapping("{id}")
    @PreAuthorize("hasAuthority('USER_ROLE')")
    public ResponseEntity<CampaignResponseCreate> updateCampaign(@PathVariable("id") Long id,
                                                                 @Valid @RequestBody CampaignDTO campaignDTO){
        campaignDTO.setId(id);
        CampaignResponseCreate response = service.update(campaignDTO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Operation(
            description = "Upload endpoint for Upload Campaign",
            summary = "This is a summary for Campaign upload endpoint",
            responses = {
                    @ApiResponse(
                            description = "Success",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Unauthorized / Invalid Token",
                            responseCode = "403"
                    )
            }

    )

    @PatchMapping(value = "/{campaignId}/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @PreAuthorize("hasAuthority('USER_ROLE')")
    public ResponseEntity<CampaignImageResponse> upload(
            @PathVariable Long campaignId,
            @RequestPart("campaignImage") MultipartFile campaignImage, CampaignImageDTO campaignImageDTO) throws IOException {
        campaignImageDTO.setCampaignId(campaignId);
        CampaignImageResponse response = service.upload(campaignImage, campaignImageDTO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
