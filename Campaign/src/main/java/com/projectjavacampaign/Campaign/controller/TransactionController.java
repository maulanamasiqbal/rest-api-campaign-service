package com.projectjavacampaign.Campaign.controller;

import com.midtrans.httpclient.error.MidtransError;
import com.projectjavacampaign.Campaign.dto.TransactionDTO;
import com.projectjavacampaign.Campaign.dto.response.ListTransaction;
import com.projectjavacampaign.Campaign.dto.response.TransactionResponse;
import com.projectjavacampaign.Campaign.service.TransactionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Tag(
        name = "Create, Get Transaction REST API for Transactions Resource",
        description = "Create, Get Transaction Rest APIs - Create Transaction"
)
@RestController
@AllArgsConstructor
@RequestMapping("api/v1/transactions")
public class TransactionController {

    private TransactionService transactionService;

    @Operation(
            summary = "Create or Register Transaction API",
            description = "Create Transaction REST API is used save transaction in a database"
    )
    @ApiResponse(
            responseCode = "201",
            description = "HTTP Status 201 CREATE"
    )
    @PostMapping("/")
    @PreAuthorize("hasAuthority('USER_ROLE')")
    public ResponseEntity<TransactionResponse> createTransaction(@Valid @RequestBody TransactionDTO transactionDTO) throws MidtransError {
        TransactionResponse response = transactionService.createTransaction(transactionDTO);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Operation(
            summary = "Get All Transaction API",
            description = "Get All Transaction REST API is used egt transaction in a database"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP Status 200 OK"
    )
    @GetMapping("/")
    @PreAuthorize("hasAuthority('USER_ROLE')")
    public ResponseEntity<ListTransaction> getTransaction(){
        ListTransaction all = transactionService.list();
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    @Operation(
            summary = "Get By Campaign Id Transaction API",
            description = "Get By Id Campaign Transaction REST API is used egt transaction in a database"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP Status 200 OK"
    )
    @GetMapping("/campaigns/{id}")
    @PreAuthorize("hasAuthority('USER_ROLE')")
    public ResponseEntity<ListTransaction> getCampaignByIDTransaction(@PathVariable("id")
                                                                      Long id){
        ListTransaction findById = transactionService.getByCampaignId(id);
        return new ResponseEntity<>(findById, HttpStatus.OK);
    }
}
