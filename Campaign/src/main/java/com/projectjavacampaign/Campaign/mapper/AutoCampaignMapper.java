package com.projectjavacampaign.Campaign.mapper;

import com.projectjavacampaign.Campaign.dto.CampaignDTO;
import com.projectjavacampaign.Campaign.dto.response.CampaignResponseCreate;
import com.projectjavacampaign.Campaign.entity.Campaign;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AutoCampaignMapper {

    AutoCampaignMapper MAPPER = Mappers.getMapper(AutoCampaignMapper.class);

    CampaignResponseCreate mapToCampaignDto(Campaign campaign);

    Campaign mapToCampaign(CampaignDTO campaignDTO);
}
