package com.projectjavacampaign.Campaign.mapper;

import com.projectjavacampaign.Campaign.dto.response.UserResponse;
import com.projectjavacampaign.Campaign.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AutoUserMapper {

    AutoUserMapper MAPPER = Mappers.getMapper(AutoUserMapper.class);

    UserResponse mapToUserDto(User user);
}
