package com.projectjavacampaign.Campaign.mapper;

import com.projectjavacampaign.Campaign.dto.response.TransactionResponse;
import com.projectjavacampaign.Campaign.entity.Transaction;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AutoTransactionMapper {

    AutoTransactionMapper MAPPER = Mappers.getMapper(AutoTransactionMapper.class);

    TransactionResponse mapToTransactionDto(Transaction transaction);
}
