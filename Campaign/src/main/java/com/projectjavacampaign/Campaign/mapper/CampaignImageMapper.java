package com.projectjavacampaign.Campaign.mapper;

import com.projectjavacampaign.Campaign.dto.response.CampaignImageResponse;
import com.projectjavacampaign.Campaign.entity.CampaignImage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CampaignImageMapper {

    CampaignImageMapper MAPPER = Mappers.getMapper(CampaignImageMapper.class);

    CampaignImageResponse mapToCampaignDto(CampaignImage campaignImage);
}
