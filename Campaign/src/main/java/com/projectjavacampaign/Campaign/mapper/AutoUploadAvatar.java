package com.projectjavacampaign.Campaign.mapper;

import com.projectjavacampaign.Campaign.dto.response.CampaignImageResponse;
import com.projectjavacampaign.Campaign.dto.response.UploadResponse;
import com.projectjavacampaign.Campaign.entity.CampaignImage;
import com.projectjavacampaign.Campaign.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AutoUploadAvatar {

    AutoUploadAvatar MAPPER = Mappers.getMapper(AutoUploadAvatar.class);

    UploadResponse mapToUpload(User user);

    CampaignImageResponse mapToCampaignDto(CampaignImage campaignImage);
}
