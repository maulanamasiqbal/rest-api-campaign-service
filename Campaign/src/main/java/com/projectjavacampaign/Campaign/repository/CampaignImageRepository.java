package com.projectjavacampaign.Campaign.repository;

import com.projectjavacampaign.Campaign.entity.CampaignImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CampaignImageRepository extends JpaRepository<CampaignImage, Long> {

    @Transactional
    @Query("select ci.fileName, ci.isPrimary  from CampaignImage ci where ci.campaign.id = :id and ci .isPrimary = true")
    List<Object[]> getCampaignImage(@Param("id") Long id);
}
