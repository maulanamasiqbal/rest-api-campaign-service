package com.projectjavacampaign.Campaign.repository;

import com.projectjavacampaign.Campaign.entity.Campaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CampaignRepository extends JpaRepository<Campaign, Long> {

    @Transactional
    @Query(value = "select c.id, c.name , c.short_description , c.backer_count , c.goal_amount , c.current_amount , c.slug, c.user_id , \n" +
            "coalesce((select ci.file_name from campaign_images ci where ci.campaign_id = c.id and ci.is_primary = true limit 1),'') as file_name \n" +
            "from campaigns c", nativeQuery = true)
    List<Object[]> findCampaign();
}
