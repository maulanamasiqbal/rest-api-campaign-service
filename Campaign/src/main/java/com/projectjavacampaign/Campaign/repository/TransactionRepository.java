package com.projectjavacampaign.Campaign.repository;

import com.projectjavacampaign.Campaign.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Transactional
    @Query(value = "select t.id, t.status, t.amount, t.created_time from Transaction t")
    List<Object[]> findList();

    @Transactional
    @Query(value = "select t.id, t.user.name, t.amount, t.created_time from Transaction t where t.campaign.id = :id")
    List<Object[]> findByCampaignId(@Param("id") Long id);

    @Transactional
    @Query(value = "select count(*) from transaction where DATE_PART('year', created_time) = :year", nativeQuery = true)
    int jumlahTransaction(@Param("year") int year);

    @Transactional
    @Query(value = "select count(code) from transaction where code = :code", nativeQuery = true)
    int cekCode(@Param("code") String code);
}
