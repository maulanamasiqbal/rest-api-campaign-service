package com.projectjavacampaign.Campaign.service;

import com.projectjavacampaign.Campaign.dto.AuthDTO;
import com.projectjavacampaign.Campaign.dto.UserDTO;
import com.projectjavacampaign.Campaign.dto.response.AuthResponse;
import com.projectjavacampaign.Campaign.dto.response.UploadResponse;
import com.projectjavacampaign.Campaign.dto.response.UserResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface UserService {

    UserResponse createUser(UserDTO userDTO);

    AuthResponse login(AuthDTO authDTO);

    UploadResponse uploadAvatar(MultipartFile avatarFileName, Long id) throws IOException;
}
