package com.projectjavacampaign.Campaign.service;

import com.midtrans.httpclient.error.MidtransError;
import com.projectjavacampaign.Campaign.dto.TransactionDTO;
import com.projectjavacampaign.Campaign.dto.response.ListTransaction;
import com.projectjavacampaign.Campaign.dto.response.TransactionResponse;

public interface TransactionService {

    TransactionResponse createTransaction(TransactionDTO transactionDTO) throws MidtransError;

    ListTransaction list();

    ListTransaction getByCampaignId(Long id);
}
