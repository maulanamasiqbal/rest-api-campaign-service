package com.projectjavacampaign.Campaign.service;

import com.projectjavacampaign.Campaign.dto.CampaignDTO;
import com.projectjavacampaign.Campaign.dto.CampaignImageDTO;
import com.projectjavacampaign.Campaign.dto.response.CampaignImageResponse;
import com.projectjavacampaign.Campaign.dto.response.CampaignResponseCreate;
import com.projectjavacampaign.Campaign.dto.response.ListCampaignResponse;
import com.projectjavacampaign.Campaign.dto.response.ResponseCampaign;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface CampaignService {

    CampaignResponseCreate createCampaign(CampaignDTO campaignDTO);

    ListCampaignResponse getCampaignAll();

    ResponseCampaign getByID(Long id);

    CampaignResponseCreate update(CampaignDTO campaignDTO);

    CampaignImageResponse upload(MultipartFile campaignImage, CampaignImageDTO campaignImageDTO) throws IOException;
}
