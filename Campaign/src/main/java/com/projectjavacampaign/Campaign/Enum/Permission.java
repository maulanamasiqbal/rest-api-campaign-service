package com.projectjavacampaign.Campaign.Enum;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Permission {

    USER_UPLOAD("user:upload"),
    USER_CREATE("user:create"),
    USER_READ("user:read"),
    USER_UPDATE("user:update")
    ;

    @Getter
    private final String permission;

}
