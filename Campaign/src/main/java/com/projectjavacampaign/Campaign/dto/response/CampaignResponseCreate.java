package com.projectjavacampaign.Campaign.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CampaignResponseCreate {

    private Long id;

    private String name;

    private String shortDescription;

    private Integer backerCount;

    private Integer goalAmount;

    private Integer currentAmount;

    private String slug;

    private Long userId;
    private String fileName;
}
