package com.projectjavacampaign.Campaign.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionResponse {

    private Long campaignId;
    private Integer amount;
    private Long userId;
    private String paymentURL;
    private String status;
    private Long id;
    private String code;
}
