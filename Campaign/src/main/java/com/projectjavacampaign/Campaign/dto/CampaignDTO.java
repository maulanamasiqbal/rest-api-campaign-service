package com.projectjavacampaign.Campaign.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CampaignDTO {

    private Long id;

    @Schema(description = "name")
    @NotEmpty(message = "name should not be null or empty")
    private String name;

    @Schema(description = "shortDescription")
    @NotEmpty(message = "shortDescription should not be null or empty")
    private String shortDescription;

    @Schema(description = "description")
    @NotEmpty(message = "description should not be null or empty")
    private String description;

    @Schema(description = "goal_amount")
    @NotNull(message = "goalAmount should not be null or empty")
    private Integer goalAmount;

    @Schema(description = "perks")
    @NotEmpty(message = "perks should not be null or empty")
    private String perks;
}
