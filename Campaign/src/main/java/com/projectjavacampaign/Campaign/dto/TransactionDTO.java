package com.projectjavacampaign.Campaign.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDTO {

    @Schema(name = "campaignId")
    @NotNull(message = "campaignId not null")
    private Long campaignId;

    @Schema(name = "amount")
    @NotNull(message = "amount not null")
    private Integer amount;
}
