package com.projectjavacampaign.Campaign.dto;

import com.projectjavacampaign.Campaign.Enum.EnumRole;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

@Schema(description = "User DTO Model Information")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDTO {

    @Schema(description = "name")
    @NotEmpty(message = "name should not be null or empty")
    private String name;

    @Schema(description = "occupation")
    @NotEmpty(message = "occupation should not be null or empty")
    private String occupation;

    @Schema(description = "email")
    @NotEmpty(message = "email should not be null or empty")
    @Email(message = "Email address should be valid")
    private String email;

    @Schema(description = "password")
    @NotEmpty(message = "password should not be null or empty")
    private String password;
}
