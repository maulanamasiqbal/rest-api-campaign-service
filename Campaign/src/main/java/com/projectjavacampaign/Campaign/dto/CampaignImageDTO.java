package com.projectjavacampaign.Campaign.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CampaignImageDTO {

    @Schema(name = "campaign_id")
    @NotNull(message = "campaigId not null")
    private Long campaignId;

    @Schema(name = "isPrimary")
    private Boolean isPrimary;
}
