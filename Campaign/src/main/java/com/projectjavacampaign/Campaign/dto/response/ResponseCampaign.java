package com.projectjavacampaign.Campaign.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseCampaign {

    private DetailCampaignResponse detailCampaignResponse;
    private UserCampaignResponse userCampaignResponse;
    private List<CampaignImageResponse> campaignImageResponses;
}
