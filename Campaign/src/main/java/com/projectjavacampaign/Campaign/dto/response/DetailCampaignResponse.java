package com.projectjavacampaign.Campaign.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DetailCampaignResponse {

    private Long id;

    private String name;

    private String shortDescription;

    private Integer goalAmount;

    private Integer currentAmount;

    private String slug;
    private String perks;
}
