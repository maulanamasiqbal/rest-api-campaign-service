package com.projectjavacampaign.Campaign.serviceImpl;

import com.projectjavacampaign.Campaign.Enum.EnumRole;
import com.projectjavacampaign.Campaign.config.JwtService;
import com.projectjavacampaign.Campaign.dto.AuthDTO;
import com.projectjavacampaign.Campaign.dto.UserDTO;
import com.projectjavacampaign.Campaign.dto.response.AuthResponse;
import com.projectjavacampaign.Campaign.dto.response.UploadResponse;
import com.projectjavacampaign.Campaign.dto.response.UserResponse;
import com.projectjavacampaign.Campaign.entity.User;
import com.projectjavacampaign.Campaign.exception.BadRequestException;
import com.projectjavacampaign.Campaign.exception.EmailAlreadyExistsException;
import com.projectjavacampaign.Campaign.exception.ResourceNotFoundException;
import com.projectjavacampaign.Campaign.mapper.AutoUploadAvatar;
import com.projectjavacampaign.Campaign.mapper.AutoUserMapper;
import com.projectjavacampaign.Campaign.repository.UserRepository;
import com.projectjavacampaign.Campaign.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Value("${app.upload-file-path}")
    private String uploadPath;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public UserResponse createUser(UserDTO userDTO) {

        Optional<User> optionalUser = userRepository.findByEmail(userDTO.getEmail());
        if (optionalUser.isPresent()){
            throw new EmailAlreadyExistsException("Email Already Exists for User");
        }

        var user = User.builder()
                .name(userDTO.getName())
                .occupation(userDTO.getOccupation())
                .email(userDTO.getEmail())
                .password(passwordEncoder.encode(userDTO.getPassword()))
                .created_time(new Date())
                .role(EnumRole.USER_ROLE)
                .build();
        userRepository.save(user);

        UserResponse userResponse = AutoUserMapper.MAPPER.mapToUserDto(user);
        return userResponse;
    }

    @Override
    public AuthResponse login(AuthDTO authDTO) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authDTO.getEmail(),
                        authDTO.getPassword()
                )
        );

        var user = userRepository.findByEmail(authDTO.getEmail()).orElseThrow();
        var jwtToken = jwtService.generateToken(user);

        return AuthResponse.builder()
                .accessToken(jwtToken)
                .build();
    }

    @Override
    public UploadResponse uploadAvatar(MultipartFile avatarFileName, Long id) throws IOException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Long loggedInUserId = null;

        if (authentication != null && authentication.getPrincipal() instanceof User){
            User userPrincipal = (User) authentication.getPrincipal();
            loggedInUserId = userPrincipal.getId();
        }

        if (loggedInUserId == null || !loggedInUserId.equals(id)) {
            throw new BadRequestException("User", "user", loggedInUserId);
        }

        User user = userRepository.findById(id).orElseThrow(
                    () -> new ResourceNotFoundException("User", "user", id)
            );

        String filename = user.getId() + "_" + avatarFileName.getOriginalFilename();
        Path filePath = Path.of(uploadPath, filename);

        Files.copy(avatarFileName.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);

        user.setAvatarFileName(filename);
        user.setUpdated_time(new Date());
        var uploadFile = userRepository.save(user);

        UploadResponse response = AutoUploadAvatar.MAPPER.mapToUpload(uploadFile);

        return response;
    }
}
