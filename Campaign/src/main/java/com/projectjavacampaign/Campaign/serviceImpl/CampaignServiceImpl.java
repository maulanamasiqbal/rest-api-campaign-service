package com.projectjavacampaign.Campaign.serviceImpl;

import com.projectjavacampaign.Campaign.dto.CampaignDTO;
import com.projectjavacampaign.Campaign.dto.CampaignImageDTO;
import com.projectjavacampaign.Campaign.dto.response.*;
import com.projectjavacampaign.Campaign.entity.Campaign;
import com.projectjavacampaign.Campaign.entity.CampaignImage;
import com.projectjavacampaign.Campaign.entity.User;
import com.projectjavacampaign.Campaign.exception.BadRequestException;
import com.projectjavacampaign.Campaign.exception.ResourceNotFoundException;
import com.projectjavacampaign.Campaign.mapper.AutoCampaignMapper;
import com.projectjavacampaign.Campaign.mapper.CampaignImageMapper;
import com.projectjavacampaign.Campaign.repository.CampaignImageRepository;
import com.projectjavacampaign.Campaign.repository.CampaignRepository;
import com.projectjavacampaign.Campaign.repository.UserRepository;
import com.projectjavacampaign.Campaign.service.CampaignService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class CampaignServiceImpl implements CampaignService {

    private CampaignRepository campaignRepository;
    private UserRepository userRepository;
    private CampaignImageRepository campaignImageRepository;

    @Override
    public CampaignResponseCreate createCampaign(CampaignDTO campaignDTO) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User user = new User();

        if (authentication != null && authentication.getPrincipal() instanceof User loggedUserId){
            user.setId(loggedUserId.getId());
        }

        Campaign campaign = AutoCampaignMapper.MAPPER.mapToCampaign(campaignDTO);
        campaign.setDescription(campaignDTO.getDescription());
        campaign.setCreated_time(new Date());
        campaign.setUser(user);

        String slugName = campaign.getName() + "-" + campaign.getUser().getId();
        campaign.setSlug(slugName);

        Campaign saveCampaign = campaignRepository.save(campaign);

        CampaignResponseCreate response = AutoCampaignMapper.MAPPER.mapToCampaignDto(saveCampaign);
        response.setUserId(campaign.getUser().getId());

        return response;
    }

    @Override
    public ListCampaignResponse getCampaignAll() {
        List<Object[]> all = campaignRepository.findCampaign();
        List<CampaignResponseCreate> list = new ArrayList<>();

        for (Object[] campaign : all) {
            CampaignResponseCreate responseCreate = new CampaignResponseCreate();
            responseCreate.setId((Long) campaign[0]);
            responseCreate.setName((String) campaign[1]);
            responseCreate.setShortDescription((String) campaign[2]);
            responseCreate.setBackerCount((Integer) campaign[3]);
            responseCreate.setGoalAmount((Integer) campaign[4]);
            responseCreate.setCurrentAmount((Integer) campaign[5]);
            responseCreate.setSlug((String) campaign[6]);
            responseCreate.setUserId((Long) campaign[7]);
            responseCreate.setFileName((String) campaign[8]);
            list.add(responseCreate);
        }

        ListCampaignResponse response = new ListCampaignResponse();
        response.setResponse(list);

        return response;
    }

    @Override
    public ResponseCampaign getByID(Long id) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {

            Long loggedUserId = ((User) authentication.getPrincipal()).getId();

            Campaign campaign = campaignRepository.findById(id).orElseThrow(
                    () -> new ResourceNotFoundException("Campaign", "campaign", id)
            );

            User user = userRepository.findById(campaign.getUser().getId()).orElseThrow(
                    () -> new ResourceNotFoundException("User", "userId", campaign.getUser().getId())
            );

            List<Object[]> campaignImages = campaignImageRepository.getCampaignImage(id);
            List<CampaignImageResponse> list = new ArrayList<>();

            if (!loggedUserId.equals(user.getId())) {
                throw new BadRequestException("Campaign", "campaignUserId", loggedUserId);
            }

            String[] perksArray = campaign.getPerks().split(",");

            List<String> perksList = new ArrayList<>();

            for (String s : perksArray){
                String formatPerks = s.trim().toLowerCase();
                perksList.add(formatPerks);
            }

            for (Object[] campaignImage : campaignImages) {
                CampaignImageResponse campaignImageResponse = new CampaignImageResponse();
                campaignImageResponse.setFileName((String) campaignImage[0]);
                campaignImageResponse.setIsPrimary((Boolean) campaignImage[1]);
                list.add(campaignImageResponse);
            }

            DetailCampaignResponse campaignResponse = new DetailCampaignResponse(
                    campaign.getId(),
                    campaign.getName(),
                    campaign.getShortDescription(),
                    campaign.getGoalAmount(),
                    campaign.getCurrentAmount(),
                    campaign.getSlug(),
                    campaign.getPerks()
            );

            campaignResponse.setPerks(perksList.toString());

            UserCampaignResponse userCampaignResponse = new UserCampaignResponse(
                    user.getName(),
                    user.getAvatarFileName()
            );

            ResponseCampaign response = new ResponseCampaign();
            response.setCampaignImageResponses(list);
            response.setDetailCampaignResponse(campaignResponse);
            response.setUserCampaignResponse(userCampaignResponse);

            return response;
        } else {
            return null;
        }
    }

    @Override
    public CampaignResponseCreate update(CampaignDTO campaignDTO) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {

            Long loggedUserId = ((User) authentication.getPrincipal()).getId();

            Campaign existingCampaign = campaignRepository.findById(campaignDTO.getId()).orElseThrow(
                    () -> new ResourceNotFoundException("Campaign", "campaign", campaignDTO.getId())
            );

            if (!loggedUserId.equals(existingCampaign.getUser().getId())) {
                throw new BadRequestException("Campaign", "campaignUserId", loggedUserId);
            }

            existingCampaign.setName(campaignDTO.getName());
            existingCampaign.setShortDescription(campaignDTO.getShortDescription());
            existingCampaign.setDescription(campaignDTO.getDescription());
            existingCampaign.setGoalAmount(campaignDTO.getGoalAmount());
            existingCampaign.setPerks(campaignDTO.getPerks());
            existingCampaign.setUpdated_time(new Date());
            String slugName = existingCampaign.getName() + "-" + existingCampaign.getUser().getId();
            existingCampaign.setSlug(slugName);

            Campaign campaign = campaignRepository.save(existingCampaign);

            CampaignResponseCreate response = AutoCampaignMapper.MAPPER.mapToCampaignDto(campaign);
            response.setUserId(campaign.getUser().getId());
            return response;

        }else {
            return null;
        }
    }

    @Override
    public CampaignImageResponse upload(MultipartFile campaignImage, CampaignImageDTO campaignImageDTO) throws IOException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {

            Long loggedUserId = ((User) authentication.getPrincipal()).getId();

            Campaign campaign = campaignRepository.findById(campaignImageDTO.getCampaignId()).orElseThrow(
                    () -> new ResourceNotFoundException("Campaign", "campaign", campaignImageDTO.getCampaignId())
            );

            if (!loggedUserId.equals(campaign.getUser().getId())) {
                throw new BadRequestException("Campaign", "campaignUserId", loggedUserId);
            }

            CampaignImage campaignImages = new CampaignImage();
            campaignImages.setCampaign(campaign);
            campaignImages.setPrimary(campaignImageDTO.getIsPrimary());
            campaignImages.setCreated_time(new Date());

            String uploadPath = "Campaign/src/main/upload/";

            String fileName = "images/" + campaign.getId() + "-" + campaignImage.getOriginalFilename();
            Path filePath = Path.of(uploadPath, fileName);

            Files.copy(campaignImage.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
            campaignImages.setFileName(fileName);

            var image = campaignImageRepository.save(campaignImages);

            CampaignImageResponse response = CampaignImageMapper.MAPPER.mapToCampaignDto(image);
            return response;
        } else {
            return null;
        }
    }
}