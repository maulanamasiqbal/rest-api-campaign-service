package com.projectjavacampaign.Campaign.serviceImpl;

import com.midtrans.Config;
import com.midtrans.ConfigFactory;
import com.midtrans.httpclient.error.MidtransError;
import com.midtrans.service.MidtransSnapApi;
import com.projectjavacampaign.Campaign.dto.PaymentDTO;
import com.projectjavacampaign.Campaign.dto.TransactionDTO;
import com.projectjavacampaign.Campaign.dto.response.ListTransaction;
import com.projectjavacampaign.Campaign.dto.response.TransactionByCampaignResponse;
import com.projectjavacampaign.Campaign.dto.response.TransactionDetailResponse;
import com.projectjavacampaign.Campaign.dto.response.TransactionResponse;
import com.projectjavacampaign.Campaign.entity.Campaign;
import com.projectjavacampaign.Campaign.entity.Transaction;
import com.projectjavacampaign.Campaign.entity.User;
import com.projectjavacampaign.Campaign.exception.BadRequestException;
import com.projectjavacampaign.Campaign.exception.ResourceNotFoundException;
import com.projectjavacampaign.Campaign.mapper.AutoTransactionMapper;
import com.projectjavacampaign.Campaign.repository.CampaignRepository;
import com.projectjavacampaign.Campaign.repository.TransactionRepository;
import com.projectjavacampaign.Campaign.repository.UserRepository;
import com.projectjavacampaign.Campaign.service.TransactionService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@AllArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private TransactionRepository transactionRepository;
    private CampaignRepository campaignRepository;
    private UserRepository userRepository;

    @Override
    public TransactionResponse createTransaction(TransactionDTO transactionDTO) throws MidtransError {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {

            Long loggedUserId = ((User) authentication.getPrincipal()).getId();

            Campaign campaign = campaignRepository.findById(transactionDTO.getCampaignId()).orElseThrow(
                    () -> new ResourceNotFoundException("Campaign", "campaignId", transactionDTO.getCampaignId())
            );

            User user = userRepository.findById(campaign.getUser().getId()).orElseThrow(
                    () -> new ResourceNotFoundException("User", "userId", campaign.getUser().getId())
            );

            if (!loggedUserId.equals(user.getId())) {
                throw new BadRequestException("Campaign", "campaignUserId", loggedUserId);
            }

            user.setId(loggedUserId);

            Transaction transaction = new Transaction();
            transaction.setCampaign(campaign);
            transaction.setAmount(transactionDTO.getAmount());
            transaction.setCode(generateCode());
            transaction.setCreated_time(new Date());
            transaction.setUser(user);
            transaction.setStatus("pending");

            var save = transactionRepository.save(transaction);

            PaymentDTO payment = new PaymentDTO(
                    save.getCode(),
                    save.getAmount().longValue()
            );

            var payments = GetPayment(payment, user);

            save.setPaymentURL(payments);

            var newSaved = transactionRepository.save(save);

            TransactionResponse response = AutoTransactionMapper.MAPPER.mapToTransactionDto(newSaved);
            response.setCampaignId(transaction.getCampaign().getId());
            response.setUserId(transaction.getUser().getId());
            response.setPaymentURL(newSaved.getPaymentURL());

            return response;
        }
        return null;
    }

    @Override
    public ListTransaction list() {

        List<Object[]> all = transactionRepository.findList();
        List<TransactionDetailResponse> list = new ArrayList<>();

        for (Object[] transactions : all) {
            TransactionDetailResponse responses = new TransactionDetailResponse();
            responses.setId((Long) transactions[0]);
            responses.setStatus((String) transactions[1]);
            responses.setAmount((Integer) transactions[2]);
            responses.setCreated_time((Date) transactions[3]);
            list.add(responses);
        }

        ListTransaction response = new ListTransaction();
        response.setResponses(list);

        return response;
    }

    @Override
    public ListTransaction getByCampaignId(Long id) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {

            Long loggedUserId = ((User) authentication.getPrincipal()).getId();

            Campaign campaign = campaignRepository.findById(id).orElseThrow(
                    () -> new ResourceNotFoundException("Campaign", "campaign", id)
            );

            User user = userRepository.findById(campaign.getUser().getId()).orElseThrow(
                    () -> new ResourceNotFoundException("User", "userId", campaign.getUser().getId())
            );

            List<Object[]> findByCampaign = transactionRepository.findByCampaignId(id);
            List<TransactionByCampaignResponse> list = new ArrayList<>();

            if (!loggedUserId.equals(user.getId())) {
                throw new BadRequestException("Campaign", "campaignUserId", loggedUserId);
            }

            for (Object[] transactions : findByCampaign) {
                TransactionByCampaignResponse responses = new TransactionByCampaignResponse();
                responses.setId((Long) transactions[0]);
                responses.setName((String) transactions[1]);
                responses.setAmount((Integer) transactions[2]);
                responses.setCreated_time((Date) transactions[3]);
                list.add(responses);
            }

            ListTransaction response = new ListTransaction();
            response.setResponse(list);
            return response;
        }
        return null;
    }

    public String GetPayment(PaymentDTO transaction, User user) throws MidtransError {

        MidtransSnapApi snapApi = new ConfigFactory(new Config("SECRET-KEY",
                "CLIENT-KEY",
                false))
                .getSnapApi();

        Map<String, Object> params = new HashMap<>();

        Map<String, Object> transactionDetails = new HashMap<>();
        transactionDetails.put("order_id", transaction.getOrder_id());
        transactionDetails.put("gross_amount", transaction.getGross_amount());

        Map<String, Object> customerDetails = new HashMap<>();
        customerDetails.put("first_name", user.getName());
        customerDetails.put("email", user.getEmail());

        params.put("transaction_details", transactionDetails);
        params.put("customer_details", customerDetails);

        String redirectURL = snapApi.createTransactionRedirectUrl(params);

        return redirectURL;
    }

    private String generateCode(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMyyyy");
        LocalDateTime now = LocalDateTime.now();
        int year =now.getYear();
        int countTransaction = transactionRepository.jumlahTransaction(year);
        String code = ("ORDER-" + dtf.format(now) + String.format("%03d", countTransaction+1));
        while (transactionRepository.cekCode(code) > 0){
            code = ("ORDER-" + dtf.format(now) + String.format("%03d", countTransaction++));
        }
        return code;
    }
}
